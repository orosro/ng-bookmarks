import {MediaMatcher} from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import MockBookmarks  from './mock-bookmarks.json';
import { Bookmark, Group } from './models/bookmarks.types';
import * as fromStore from './store';
import {Store} from '@ngrx/store';
import { Observable } from 'rxjs';

import {MatBottomSheet} from '@angular/material/bottom-sheet';
import {BkMBottomSheetComponent} from './components/bk-m-bottom-sheet/bk-m-bottom-sheet.component';

@Component({
  selector: 'ng-bookmarks-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  mobileQuery: MediaQueryList;

  bookmarks$: Observable<Bookmark[]>;
  groups$: Observable<Group[]>;

  private _mobileQueryListener: () => void;

  fillerNav = Array.from({length: 10}, (_, i) => `Nav Item ${i + 1}`);

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private media: MediaMatcher,
    private _bottomSheet: MatBottomSheet,
    private store: Store<fromStore.BookmarksAppState>
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 768px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit(): void {
    // Dispatch initial load actions
    this.store.dispatch(fromStore.loadBookmarks());
    this.store.dispatch(fromStore.loadGroups());


    this.bookmarks$ = this.store.select(fromStore.getAllBookmarks);
    this.groups$ = this.store.select(fromStore.getAllGroups);
    // TODO: Implement load groups logic
    // this.groups$ = this.store.select(fromStore.getAllGroups);
  }
  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  selectBookmark() {
    // TODO: update selected bookmark in app state and populate form based on it
  }

  addBookmark(bookmark: Bookmark) {
    this.store.dispatch(fromStore.addBookmark({
      payload: bookmark
    }))
  }

  removeBookmark(bookmarkId){
    this.store.dispatch(fromStore.removeBookmark({
      payload: bookmarkId
    }))
  }




}

