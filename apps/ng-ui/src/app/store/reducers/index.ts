import * as fromBookmarks from './bookmarks.reducer';
import * as fromGroups from './groups.reducer';
import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';


export interface BookmarksAppState {
  bookmarks: fromBookmarks.BookmarksState;
  groups: fromGroups.GroupsState;
}

export const reducers: ActionReducerMap<BookmarksAppState> = {
  bookmarks: fromBookmarks.bookmarksReducer,
  groups: fromGroups.groupsReducer,
}

export const getBookmarksState = createFeatureSelector<BookmarksAppState, fromBookmarks.BookmarksState>('bookmarks')
export const getGroupsState = createFeatureSelector<BookmarksAppState, fromGroups.GroupsState>('groups')

