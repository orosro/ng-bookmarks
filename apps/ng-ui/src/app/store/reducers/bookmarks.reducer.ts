import { createReducer, on, Action } from '@ngrx/store';
import { removeBookmarkSuccess, editBookmarkSuccess, loadBookmarksSuccess, addBookmarkSuccess } from '../actions/bookmarks.actions';
import {Bookmark} from '../../models/bookmarks.types';


export interface BookmarksState {
    entities: Bookmark[];
    loaded: boolean; // TODO: Enhance application to use load & loading properties
    loading: boolean;
}

export const initialState: BookmarksState = {
  entities: [],
  loaded: false,
  loading: false
};

const _bookmarksReducer = createReducer(
  initialState,
  on(loadBookmarksSuccess, onLoadBookmarksSuccess),
  on(addBookmarkSuccess, onAddBookmark),
  on(removeBookmarkSuccess, onRemoveBookmark),
  on(editBookmarkSuccess, onEditBookmark)
);


function onLoadBookmarksSuccess(state, {payload: bookmarks } ) {
  return {...state, ...{entities: bookmarks}}
}

function onAddBookmark(state, {payload: bookmark}) {
  const entities = [...state.entities]
  entities.push(bookmark);
  return {...state, entities};
}

function onRemoveBookmark(state, {payload: bookmark}) {
  const entities = [...state.entities].filter(bk => bk.id !== bookmark.id);
  return {...state, entities};
}

function onEditBookmark(state, {payload: bookmark}) {
  const entities = [...state.entities];
  const bkId = entities.findIndex(bk => bk.id === bookmark.id);
  entities[bkId] = bookmark;
  return {...state, entities};
}

export function bookmarksReducer(state: BookmarksState | undefined, action: Action) {
  return _bookmarksReducer(state, action);
}

export const getRedBookmarksEntities = (state: BookmarksState) => state.entities;
export const getRedBookmarksLoading = (state: BookmarksState) => state.loading;
export const getRedBookmarksLoaded = (state: BookmarksState) => state.loaded;
