import { createReducer, on, Action } from '@ngrx/store';
import { loadGroupsSuccess } from '../actions/groups.actions';
import {Group} from '../../models/bookmarks.types';


export interface GroupsState {
  entities: Group[];
  loaded: boolean; // TODO: Enhance application to use load & loading properties
  loading: boolean;
}

export const initialState: GroupsState = {
  entities: [],
  loaded: false,
  loading: false
};

const _groupsReducer = createReducer(
  initialState,
  on(loadGroupsSuccess, onLoadGroupsSuccess),
);

function onLoadGroupsSuccess(state, {payload: groups } ) {
  return {...state, ...{entities: groups}}
}

export function groupsReducer(state: GroupsState | undefined, action: Action) {
  return _groupsReducer(state, action);
}

export const getRedGroupsEntities = (state: GroupsState) => state.entities;
export const getRedGroupsLoading = (state: GroupsState) => state.loading;
export const getRedGroupsLoaded = (state: GroupsState) => state.loaded;
