import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { map, switchMap, catchError, mergeMap } from 'rxjs/operators';
import { EMPTY} from 'rxjs';


import * as groupsActions from '../actions/groups.actions'
import { GroupsService } from '../../services/groups.service';


@Injectable()
export class GroupsEffects {
  constructor(
    private actions$: Actions,
    private groupsService: GroupsService
  ) {}


  loadGroups$ = createEffect(() => this.actions$.pipe(
    ofType(groupsActions.loadGroups),
    mergeMap(
      () => this.groupsService.getGroups()
        .pipe(
          map(groups => {
            return ({ type: groupsActions.loadGroupsSuccess.type, payload: groups })
          }),
          catchError(() => EMPTY)
        ))
    )
  );

  // addGroup$
  // deleteGroup$
  // updateGroup$


}
