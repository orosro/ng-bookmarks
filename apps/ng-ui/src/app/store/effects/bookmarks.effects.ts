import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { map, switchMap, catchError, mergeMap } from 'rxjs/operators';
import { EMPTY} from 'rxjs';


import * as bookmarkActions from '../actions/bookmarks.actions'
import { BookmarksService } from '../../services';


@Injectable()
export class BookmarksEffects {
  constructor(
    private actions$: Actions,
    private bookmarksService: BookmarksService
  ) {}

  loadBookmarks$ = createEffect(() => this.actions$.pipe(
    ofType(bookmarkActions.loadBookmarks),
    mergeMap(
      () => this.bookmarksService.getBookmarks()
      .pipe(
        map(bookmarks => {
         return ({ type: bookmarkActions.loadBookmarksSuccess.type, payload: bookmarks })
        }),
        catchError(() => EMPTY)
      ))
    )
  );

  addBookmark$ = createEffect(() => this.actions$.pipe(
    ofType(bookmarkActions.addBookmark),
    mergeMap(
      ({payload}) => {
        return this.bookmarksService.addBookmark(payload)
          .pipe(
            map(bookmark => {
              return ({ type: bookmarkActions.addBookmarkSuccess.type, payload: bookmark })
            }),
            catchError(() => EMPTY)
          )
      })
    )
  );

  deleteBookmark$ = createEffect(() => this.actions$.pipe(
    ofType(bookmarkActions.removeBookmark),
    mergeMap(
      ({payload}) => {
        console.log(payload);
        return this.bookmarksService.deleteBookmark(payload)
          .pipe(
            map(bookmark => {
              console.log(bookmark)
              return ({ type: bookmarkActions.removeBookmarkSuccess.type, payload: bookmark })
            }),
            catchError(() => EMPTY)
          )
      })
    )
  );

  updateBookmark$ = createEffect(() => this.actions$.pipe(
    ofType(bookmarkActions.editBookmark),
    mergeMap(
      ({payload}) => {
        return this.bookmarksService.updateBookmark(payload)
          .pipe(
            map(bookmark => {
              return ({ type: bookmarkActions.editBookmarkSuccess.type, payload: bookmark })
            }),
            catchError(() => EMPTY)
          )
      })
    )
  );

}
