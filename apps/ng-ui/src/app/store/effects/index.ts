import { BookmarksEffects} from './bookmarks.effects';
import { GroupsEffects} from './groups.effects';

export const effects: any[] = [BookmarksEffects, GroupsEffects];

export * from './bookmarks.effects';
export * from './groups.effects';

