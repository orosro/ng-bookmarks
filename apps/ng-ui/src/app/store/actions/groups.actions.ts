import { createAction, props } from '@ngrx/store';
import {Group} from '../../models/bookmarks.types';

export const loadGroups = createAction('[Groups] Load');
export const loadGroupsSuccess = createAction('[Groups] Load Success', props<{payload: Group[]}>());

// TODO: add group action
// TODO: edit group action
// TODO: remove group action
