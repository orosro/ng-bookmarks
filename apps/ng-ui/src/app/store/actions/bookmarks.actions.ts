import { createAction, props } from '@ngrx/store';
import {Bookmark} from '../../models/bookmarks.types';


export const loadBookmarks = createAction('[Bookmarks] Load');
export const loadBookmarksSuccess = createAction('[Bookmarks] Load Success', props<{payload: Bookmark[]}>());

export const addBookmark = createAction('[Bookmarks] Add', props<{payload: Bookmark}>());
export const addBookmarkSuccess = createAction('[Bookmarks] Add Bookmark Success', props<{payload: Bookmark}>());

export const removeBookmark = createAction('[Bookmarks] Remove', props<{payload: Bookmark}>());
export const removeBookmarkSuccess = createAction('[Bookmarks] Remove Success', props<{payload: Bookmark}>());

export const editBookmark = createAction('[Bookmarks] Edit', props<{payload: Bookmark}>());
export const editBookmarkSuccess = createAction('[Bookmarks] Edit Success', props<{payload: Bookmark}>());
