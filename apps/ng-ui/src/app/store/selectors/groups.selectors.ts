import { createSelector } from '@ngrx/store';

import * as fromReducers from '../reducers';
import * as fromGroups from '../reducers/groups.reducer';


export const _getGroupsState = createSelector(
  fromReducers.getGroupsState,
  (state: fromGroups.GroupsState) => {
   return state;
  }
);


export const getGroupsEntities = createSelector(
  _getGroupsState,
  fromGroups.getRedGroupsEntities
);

export const getAllGroups = createSelector(getGroupsEntities, entities => {
  return Object.keys(entities).map(id => entities[parseInt(id, 10)]);
});
