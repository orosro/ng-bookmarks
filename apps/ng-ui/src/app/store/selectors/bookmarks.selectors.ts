import { createSelector } from '@ngrx/store';

import * as fromReducers from '../reducers';
import * as fromBookmarks from '../reducers/bookmarks.reducer';


export const _getBookmarksState = createSelector(
  fromReducers.getBookmarksState,
  (state: fromBookmarks.BookmarksState) => state
);


export const getBookmarksEntities = createSelector(
  _getBookmarksState,
  fromBookmarks.getRedBookmarksEntities
);

export const getAllBookmarks = createSelector(getBookmarksEntities, entities => {
  return Object.keys(entities).map(id => entities[parseInt(id, 10)]);
});


export const getBookmarksLoaded = createSelector(
  _getBookmarksState,
  fromBookmarks.getRedBookmarksLoaded
);
export const getBookmarksLoading = createSelector(
  _getBookmarksState,
  fromBookmarks.getRedBookmarksLoading
);
