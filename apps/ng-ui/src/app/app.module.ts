import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';

/* Material imports */
import {MatCardModule} from '@angular/material/card';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from "@angular/material/form-field";
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {MatTableModule} from '@angular/material/table';


/* NGRX STORE */
import { StoreModule } from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {StoreDevtoolsModule} from "@ngrx/store-devtools"
import { reducers, effects } from './store';

/* Components */
import { BkNavbarComponent } from './components/bk-navbar/bk-navbar.component';
import { BkMBottomSheetComponent } from './components/bk-m-bottom-sheet/bk-m-bottom-sheet.component';
import { BkListComponent } from './components/bk-list/bk-list.component';
import { BkFormComponent } from './components/bk-form/bk-form.component';



const materialModules = [MatCardModule, MatToolbarModule, MatIconModule, MatButtonModule, MatBottomSheetModule, MatSidenavModule, MatListModule, MatTableModule, MatFormFieldModule, MatInputModule];

@NgModule({
  declarations: [AppComponent, BkNavbarComponent, BkMBottomSheetComponent, BkListComponent, BkFormComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ...materialModules,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot(effects),
    // Instrumentation must be imported after importing StoreModule (config is optional)
    StoreDevtoolsModule.instrument(),
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
