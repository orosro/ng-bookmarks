import {inject, Injectable} from '@angular/core';
import { Observable,  throwError } from 'rxjs';
import { Bookmark, Group } from '../models/bookmarks.types';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

// Move to external File
const API = {
  groups: '/api/groups'
}

@Injectable({
  providedIn: 'root'
})
export class GroupsService {
  constructor(private http: HttpClient) {}

  private handleError(error: any) {
    return throwError(error.json());
  }

  public getGroups(): Observable<Group[]> {
    return this.http
      .get<Group[]>(API.groups)
      .pipe(catchError(this.handleError))
  }

  public addGroup(payload: Group): Observable<Group> {
    return this.http
      .post<Group>(API.groups, payload)
      .pipe(catchError(this.handleError))
  }

  public updateGroup(payload: Group): Observable<Group> {
    return this.http
      .put<Group>(`${API.groups}/${payload.id}`, payload)
      .pipe(catchError(this.handleError))
  }

  public deleteGroup(payload: Group): Observable<Group> {
    return this.http
      .delete<Group>(`${API.groups}/${payload.id}`)
      .pipe(catchError(this.handleError))
  }
}
