import {inject, Injectable} from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { Bookmark } from '../models/bookmarks.types';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

const API = {
  bookmarks: '/api/bookmarks',
  groups: '/api/groups'
}

@Injectable({
  providedIn: 'root'
})
export class BookmarksService {
  constructor(private http: HttpClient) {}

  private handleError(error: any) {
    return throwError(error.json());
  }

  public getBookmarks(): Observable<Bookmark[]> {
    return this.http
      .get<Bookmark[]>(API.bookmarks)
      .pipe(catchError(this.handleError))
  }

  public addBookmark(payload: Bookmark): Observable<Bookmark> {
    return this.http
      .post<Bookmark>(API.bookmarks, payload)
      .pipe(catchError(this.handleError))
  }

  public updateBookmark(payload: Bookmark): Observable<Bookmark> {
    return this.http
      .put<Bookmark>(`${API.bookmarks}/${payload.id}`, payload)
      .pipe(catchError(this.handleError))
  }

  public deleteBookmark(payload: Bookmark): Observable<Bookmark> {
    return this.http
      .delete<Bookmark>(`${API.bookmarks}/${payload.id}`)
      .pipe((resp) => {
        console.log('Remove success')
        return of(payload)
      })
      .pipe(catchError(this.handleError))
  }
}
