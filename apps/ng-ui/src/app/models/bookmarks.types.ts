export interface Bookmark {
  id?: number;
  name: string;
  url: string;
  group: number;
  description?: string;
}

export  interface Group {
  id?: number;
  name: string;
  parentId?: number;
}
