import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BkListComponent } from './bk-list.component';

describe('BkListComponent', () => {
  let component: BkListComponent;
  let fixture: ComponentFixture<BkListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BkListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BkListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
