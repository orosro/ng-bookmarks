import { Component, Input } from '@angular/core';
import { Bookmark } from '../../models/bookmarks.types';

@Component({
  selector: 'ng-bookmarks-bk-list',
  templateUrl: './bk-list.component.html',
  styleUrls: ['./bk-list.component.scss']
})
export class BkListComponent {
  @Input() bookmarks: Bookmark[];
  @Input() onBookmarkClick: () => void = () => {};
  @Input() onAddBookmark: (T:Bookmark) => void = () => {};
  @Input() onRemoveBookmark: (T:Bookmark) => void = () => {};

  public addNewBookmark(): void {
    // TODO: Implement show new bookmark form
  }

  public removeBookmark(bookmark: Bookmark) {
    if (this.onRemoveBookmark) {
      this.onRemoveBookmark(bookmark)
    }
  }

  public editBookmark(bookmark: Bookmark) {
    // TODO: Implement edit bookmark functionality
  }
}
