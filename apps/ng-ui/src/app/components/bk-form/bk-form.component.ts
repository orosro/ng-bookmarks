import { Component, Input, OnInit, Optional } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Bookmark } from '../../models/bookmarks.types';

export interface BookmarkValues {
  id?: string,
  name: string;
  url: string;
  group: string;
}
export enum ActionType {
  ADD,
  EDIT
}

@Component({
  selector: 'ng-bookmarks-bk-form',
  templateUrl: './bk-form.component.html',
  styleUrls: ['./bk-form.component.scss']
})
export class BkFormComponent implements OnInit {
  @Input() formValues?: BookmarkValues;
  @Input() onSubmit?: (T: Bookmark) => void;

  bookmarkForm: FormGroup;
  actionType: ActionType;



  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.actionType = this.formValues ? ActionType.EDIT : ActionType.ADD;
    console.log(this.actionType)

    this.bookmarkForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      url: ['', [Validators.required]],
      group: ['', [Validators.required]]
    })
  }

  submit() {
    console.log(this.bookmarkForm.value)

    if (!this.bookmarkForm.valid) {
      return;
    }

    if(this.onSubmit) {
      this.onSubmit(this.bookmarkForm.value as Bookmark);
    }
  }

}
