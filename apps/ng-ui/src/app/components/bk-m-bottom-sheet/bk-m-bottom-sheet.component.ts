import { Component, Input, OnInit } from '@angular/core';
import {MatBottomSheetRef} from '@angular/material/bottom-sheet';
import { Bookmark } from '../../models/bookmarks.types';

@Component({
  selector: 'ng-bookmarks-bk-m-bottom-sheet',
  templateUrl: './bk-m-bottom-sheet.component.html',
  styleUrls: ['./bk-m-bottom-sheet.component.scss']
})
export class BkMBottomSheetComponent implements OnInit {


  constructor(private _bottomSheetRef: MatBottomSheetRef<BkMBottomSheetComponent>) { }

  ngOnInit(): void {

  }

  // openLink(event: MouseEvent): void {
  //   this._bottomSheetRef.dismiss();
  //   event.preventDefault();
  // }

}
