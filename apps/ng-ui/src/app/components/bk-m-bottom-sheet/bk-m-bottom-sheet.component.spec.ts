import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BkMBottomSheetComponent } from './bk-m-bottom-sheet.component';

describe('BkMBottomSheetComponent', () => {
  let component: BkMBottomSheetComponent;
  let fixture: ComponentFixture<BkMBottomSheetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BkMBottomSheetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BkMBottomSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
