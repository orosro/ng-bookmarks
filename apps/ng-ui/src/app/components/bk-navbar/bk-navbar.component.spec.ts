import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BkNavbarComponent } from './bk-navbar.component';

describe('BkNavbarComponent', () => {
  let component: BkNavbarComponent;
  let fixture: ComponentFixture<BkNavbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BkNavbarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BkNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
