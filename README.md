# NgBookmarks

![alt text](./docs/screenshot.JPG)

## Quick Start & Documentation


### Prerequisites

* Node + Npm ^v10.x

## How to start the project

1. Clone the project into a directory and navigate to it
2. Install all required dependencies using ``npm install``
3. Make yourself a coffee because installing NX takes a decade
4. Start the server ``npm run server`` this will start a json-server at `localhost:4000`
5. Start the application ``npm run serve:ui`` this will start the UI app at `localhost:4201`
6. Navigate with your browser to ``http://localhost:4021``

## Specifications

#### Technical specifications

* Mock JSON Server - Decided to have a REST API instead of GRAPHQL, altghout the solution can be easily updated to match graphql specifications
* Add proxy to dev server - all `/api/` requests will be forwarded to the correct address
* NGRX state management system
    * actions - dispatched from `container` component
    * reducers - update the store
    * effects - used to decouple & implement logic that would normally reside in the containers
    * selectors - used to select certain parts of the store

#### Business specifications

* See list of available bookmarks
* See list of available bookmarks groups
* Add new bookmark - using the form on the right side of the screen
* Delete bookmark - using the delete icon from a bookmark

## TODOs

* Complete CRUD operations for groups
* Complete CRUD operations for bookmarks
* UI Enhancements
    * Future mobile optimization
    * Toggle form when adding or editing a bookmark
    * Implement theming mechanism (dark & light theme)
* Write tests for core elements
* Migrate to graphql
